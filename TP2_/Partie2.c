#include "Partie2.h"

//saisie tableau de cha�ne v1
char **saisie_tch(int nb)
{
	int i;
	char **tch;

	tch = (char**)malloc(nb * sizeof(char));

	for (i = 0; i <= nb; i++)
	{
		printf("rentrer une chaine de caractere \n ");
		tch[i] = saisie_str_2();

	}

	return tch;
}

//saisie tableau de cha�ne v2
char ** saisie_tch2()
{
	char **tch;
	char *tmp[1000];
	int i = 0, j, choix;

	do
	{
		printf("taper 1 pour saisir une chaine ou 0 pour arreter \n");
		scanf("%d  \n", &choix);

		if (choix == 1)
		{
			printf("saisir 1 chaine \n");
			tmp[i++] = saisie_str_2();
		}
	} while (choix != 0);

	if (i == 0)
	{
		return NULL;
	}

	tch = (char**)malloc((i + 1) * sizeof(char*));

	for (j = 0; j < i; j++)
	{
		tch[j] = tmp[j];
	}
	free(tmp);
	j = j + 1;
	tch[j] = NULL;
	return tch;
}

//affichage tableau de cha�nes
void affichetch(char **tch)
{
	int i;
	for (i = 0; tch[i] != NULL; i++)
	{
		affiche(tch[i]);
	}
}

//supprime un caract�re dans un tableau de cha�ne
void delete_char_from_all_str(char*** tch, char car)
{
	int size_list, nb_deleted;
	get_nb_deleted(*tch, &nb_deleted, &size_list, car);

	//Si au moin  une cha�ne a �t� supprim�e
	if (nb_deleted != 0) {
		char** tmp = (char**)malloc(sizeof(char*) * (size_list - nb_deleted + 1));
		*(tmp + size_list - nb_deleted) = NULL;

		//Copie tout les pointeurs non NULL dans tmp
		int tmp_i = 0;
		for (int i = 0; i < size_list; i++)
			if (*(*tch + i) != NULL)
				*(tmp + tmp_i++) = *(*tch + i);

		free(*tch);
		*tch = tmp;
	}
}

//Est appel� par "delete_char_from_all_str", compte le nombre de chaine supprim� et appele la fonction de suppr�ssion
void get_nb_deleted(char** tch, int* nb_deleted, int* size_tab, char car) {
	*nb_deleted = 0;
	for (*size_tab = 0; *(tch + *size_tab) != NULL; (*size_tab)++);

	for (int j = 0; j < *size_tab; j++) {
		delete_char_from_str(tch + j, car);

		if (*(tch + j) == NULL)
			(*nb_deleted)++;
	}
}

//supprime un caract�re d'une chaine, renvoie le pointeur � null si tt les caracs supprim�s
void delete_char_from_str(char** source, char cara)
{
	int size, nb_to_delete = 0;

	for (size = 0; *(*source + size) != '\0'; size++)
		if (*(*source + size) == cara)
			nb_to_delete++;

	if (size == nb_to_delete) {
		free(*source);
		*source = NULL;
	}
	else if (nb_to_delete > 0)
	{
		int new_size = size - nb_to_delete + 1;
		char* tmp = (char*)malloc(new_size * sizeof(char));

		for (int i = 0, j = 0; *(*source + i) != '\0'; i++)
			if (*(*source + i) != cara)
				*(tmp + j++) = *(*source + i);

		tmp[new_size - 1] = '\0';

		free(*source);
		*source = tmp;
	}
}

//double un caract�re dans un tableau de cha�ne
void double_char_all_str(char** tab, char car)
{
	int i = 0;
	while (*(tab + i) != NULL)
		double_char_from_str(tab + i++, car);
}

//double un caract�re dans une cha�ne
void double_char_from_str(char** str, char c) {
	int length = 0, to_be_added = 0;
	for (length = 0; *(*str + length) != '\0'; length++)
		if (*(*str + length) == c)
			to_be_added++;

	if (to_be_added != 0) {
		char* tmp = (char*)malloc(sizeof(char) * (length + 1 + to_be_added));

		int tmp_i = 0;
		for (int i = 0; i < length; i++) {
			*(tmp + tmp_i++) = *(*str + i);
			if (*(*str + i) == c)
				*(tmp + tmp_i++) = *(*str + i);
		}
		*(tmp + tmp_i) = '\0';

		free(*str);
		*str = tmp;
	}
}

//supprime toute les cha�nes qui commence par un certain caract�re
void delete_str_start_with(char*** str_tab, char c) {
	int nb_to_delete = 0, size;
	for (size = 0; *(*str_tab + size) != NULL; size++)
		if (**(*str_tab + size) == c)
			nb_to_delete++;

	if (nb_to_delete != 0) {
		char** tmp = malloc(sizeof(char*) * size - nb_to_delete + 1);
		*(tmp + size - nb_to_delete) = NULL;

		int tmp_i = 0;
		for (int i = 0; i < size; i++)
			if (**(*str_tab + i) != c)
				*(tmp + tmp_i++) = *(*str_tab + i);
			else
				free(*(*str_tab + i));

		free(*str_tab);
		*str_tab = tmp;
	}
}