#pragma once
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdint.h>

#include "List.h"
#include "Partie1.h"
#include "Partie2.h"

char* list_to_str(List* list);
List* get_string_input();

void copie(char *s, char *d);

List* get_list_str();
void print_list_str(List* list_str);