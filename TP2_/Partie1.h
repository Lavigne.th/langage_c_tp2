#pragma once
#define _CRT_SECURE_NO_WARNINGS

#include <stdint.h>
#include <stdio.h>
#include <ctype.h>

#include "Additionnal.h"

//Exercice 1 
void saisie_str_1(char *tab);
char* saisie_str_2();
char* saisie_str_3();

void affiche(char *t);

//Exercice 2
char* supression_char(char *source, char cara);
char *dedouble(char *source, char cara);
char *dedouble_tous(char *tab);

//Exercice 3
int *comptage_char1(char *tab);
int *comptage_char2(char *t, int *p1, int *p2);
void affiche_stats1(int *t);
void affiche_stats2(int *t, int cpt1, int cpt2);