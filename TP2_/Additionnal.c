#pragma once
#include "Additionnal.h"

char* list_to_str(List* list) {
	char* str = malloc(sizeof(char) * list->length + 1);
	*(str + list->length) = '\0';
	for (int i = 0; i < list->length; i++)
		*(str + i) = *(char*)get_at(list, i);

	return str;
}

List* get_string_input() {
	char c;
	List* list = get_list(NULL, sizeof(char));
	printf("Saisissez une chaine : \n");
	while ((c = getchar()) != '\n') {
		append(list, &c);
	}
	return list;
}

//copie une chaine
void copie(char *s, char *d)
{
	int i = 0;

	for (i = 0; s[i] != '\0'; i++)
	{
		d[i] = s[i];
	}
	d[i] = '\0';
}

void free_str_array(Node* n) {
	char** data = n->data;
	free(*data);
	free(data);
}

List* get_list_str() {
	int stop = 0;
	List* list_str = get_list(&free_str_array, sizeof(char**));
	while (!stop)
	{
		printf("Saisissez une chaine : \n");
		char* data = saisie_str_2();
		append(list_str, &data);

		printf("Voulez saisir une nouvelle chaine? 0 - Oui 1 - Non \n");
		scanf("%d", &stop);
		getchar();
	}
	return list_str;
}

void print_list_str(List* list_str) { 
	for (int i = 0; i < list_str->length; i++)
		printf("Chaine %d : %s\n", i, *(char**)get_at(list_str, i));
}

