#pragma once
#define _CRT_SECURE_NO_WARNINGS

#include "Partie1.h"
#include "Additionnal.h"
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>

//Exercice 1
char **saisie_tch(int nb);
char ** saisie_tch2();
void affichetch(char **tch);

void delete_char_from_str(char** source, char cara);
void get_nb_deleted(char** tch, int* nb_deleted, int* size_tab, char car);
void delete_char_from_all_str(char*** tch, char car);

void double_char_from_str(char** str, char c);
void double_char_all_str(char** tab, char car);

void delete_str_start_with(char*** str_tab, char c);