#include "Partie1.h"

//saisie v1
void saisie_str_1(char *tab)
{
	int i;
	char c;
	i = 0;
	do {
		c = getchar();
		tab[i] = c;
		i = i + 1;
	} while (c != '\n');
	tab[i - 1] = '\0';
}

//saisie v2
char* saisie_str_2()
{
	char *tmp, *res;
	int taille;

	tmp = (char *)malloc(1000 * sizeof(char));

	saisie_str_1(tmp);
	taille = strlen(tmp) + 1;

	res = (char*)malloc(taille * sizeof(char));
	copie(tmp, res);
	free(tmp);

	return res;
}

//saisie v3
char* saisie_str_3() {

	char* str = NULL;
	int nb_char = 0;
	char c;
	while ((c = getchar()) != '\n')
	{
		char* tmp = malloc(sizeof(char) * nb_char + 2);
		for (int it = 0; it < nb_char; it++)
			*(tmp + it) = *(str + it);
		*(tmp + nb_char) = (char)c;
		*(tmp + ++nb_char) = '\0';

		if (str != NULL)
			free(str);
		str = tmp;
	}

	return str;
}

//affiche une chaine caract�re par caract�re
void affiche(char *t)
{
	while (*t != '\0')
	{
		printf("%2c ", *t);
		t++;
	}
	printf("\n");
}

//supprime un caract�re
char* supression_char(char *source, char cara)
{
	char* cible = NULL;
	int nouvelle_taille, cpt, taille;

	for (taille = 0, cpt = 0; source[taille] != '\0'; taille++)
	{
		if (source[taille] == cara)
		{
			cpt++;
		}
	}

	if (cpt != 0)
	{
		nouvelle_taille = taille - cpt + 1;

		cible = (char*)malloc(nouvelle_taille * sizeof(char));

		int cible_i = 0;
		for (int i = 0; source[i] != '\0'; i++)
		{
			if (source[i] != cara)
			{
				cible[cible_i++] = source[i];
			}
		}
		cible[cible_i] = '\0';
	}
	return cible;
}

//d�double un caract�re
char *dedouble(char *source, char cara)
{
	char *cible = NULL;
	int taille, cpt, i, j;
	for (i = 0, cpt = 0; source[i] != '\0'; i++)
	{
		if (source[i] == cara)
		{
			cpt++;
		}
	}
	if (cpt <= i)
	{
		taille = i + cpt + 1;
		cible = (char*)malloc(taille * sizeof(char));
		for (i = 0, j = 0; source[i] != '\0'; i++)
		{
			cible[j++] = source[i];
			if (source[i] == cara)
			{
				cible[j++] = source[i];
			}
		}
		cible[j] = '\0';
	}
	return cible;
}

//dedouble tout les caract�re
char *dedouble_tous(char *tab)
{
	int taille, i, j;
	char *cible;

	for (taille = 0; tab[taille] != '\0'; taille++);

	taille = (taille * 2) + 1;
	cible = (char*)malloc(taille * sizeof(char));

	for (i = 0, j = 0; tab[i] != '\0'; i++)
	{
		cible[j++] = tab[i];
		cible[j++] = tab[i];
	}
	cible[j] = '\0';

	return  cible;
}

//comptage v1
int *comptage_char1(char *tab)
{
	int indice, *fcpt, i;
	char car;

	fcpt = (int*)calloc(sizeof(int), 28);

	for (i = 0; tab[i] != '\0'; i++)
	{
		car = tolower(tab[i]);

		if ((car >= 'a') && (car <= 'z'))
		{
			indice = car - 'a';
			fcpt[indice]++;
		}
		else
		{
			fcpt[26]++;
		}
		fcpt[27] = i + 1;
	}
	return fcpt;
}

//comptage v2
int *comptage_char2(char *t, int *p1, int *p2)
{
	int indice, *fcpt, i;
	char car;

	fcpt = (int*)calloc(sizeof(int), 26);

	*p1 = *p2 = 0;

	for (i = 0; t[i] != '\0'; i++)
	{
		car = tolower(t[i]);

		if ((car >= 'a') && (car <= 'z'))
		{
			indice = car - 'a';
			fcpt[indice]++;
		}
		else
		{
			(*p1)++;
		}
		(*p2)++;
	}
	return fcpt;
}

//affichage comptage v1
void affiche_stats1(int *t)
{
	int i;
	for (i = 0; i < 26; i++)
	{
		if (t[i] != 0)
		{
			printf("il y a %d caract�re %c \n", t[i], i + 'a');
		}
	}
	printf("nombre de caract�re speciaux %d \n ", t[26]);
	printf("nombre total de caractere %d \n", t[27]);
}

//affichage comptage v2
void affiche_stats2(int *t, int cpt1, int cpt2)
{
	int i;
	for (i = 0; i < 26; i++)
	{
		if (t[i] != 0)
		{
			printf("il y a %d caractere %c \n", t[i], i + 'a');
		}
	}
	printf("nombre de caractere speciaux %d \n ", cpt1);
	printf("nombre total de caractere %d \n", cpt2);
}